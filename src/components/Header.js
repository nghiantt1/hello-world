import React, { Component } from 'react';

// ES6
class Header extends Component {
	render() {
		return (
			<div>
				<h1></h1>
				<nav className="navbar navbar-inverse">
					<div className="-fluid">
						<a className="navbar-brand">React Js</a>
						<ul className="nav navbar-nav">
							<li className="active">
								<a href="#">Home</a>
							</li>
							<li>
								<a href="#">Link</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		);
	}
}

export default Header;

// function Header() {
// 	return (
// 		<div>
// 			<h1>ABC</h1>
// 		</div>
// 	)
// }
