import React, { Component } from 'react';

class Product extends Component {
	showInforProduct(product) {
		if(product.status){
			return <h3>{ product.descreption }</h3>
		}
	}
	render() {
		var a = 5;
		var product = {
			name: 'name',
			descreption: 'description',
			status: true
		};

		var users = [
			{
				id: 1,
				name: 'User A',
				age: 19
			},
			{
				id: 2,
				name: 'User B',
				age: 18
			},
			{
				id: 3,
				name: 'User C',
				age: 19
			},
		]

		var elements = users.map((user, index) => {
			return <div key={ user.id }>
				<h2>Name: { user.name }</h2>
				<p>Age: { user.age }</p>
			</div>
		})
		return (
			<div>
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div className="thumbnail">
						<img data-src="#" alt=""></img>
						<div className="caption">
							<h3>Decal</h3>
							<p>
								...
							</p>
							<p>
								<a className="btn btn-primary">Action</a>
								<a className="btn btn-default">Action</a>
							</p>
						</div>
					</div>
					<button type="button" className="btn btn-default">button</button>
				</div>

				<h3>{ product.name }</h3>
				<h4>{ product.status ? 'A' : 'B' }</h4>
				{ this.showInforProduct(product) }<br />

				{ elements }
			</div>
		);
	}
}

export default Product;
